﻿using System;
using System.Collections.Generic;
using System.Text;

namespace dotnettask12
{
    class Student
    {
        //Some standard pros to set for the student
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime DOB { get; set; }
        public string Degree { get; set; }

        //Adding these two methods is part of generating a one to many relationship from professor to student
        public int ProfessorId { get; set; }

        public Professor Professor { get; set; }

    }
}
