﻿using System;
using System.Collections.Generic;
using System.Text;

namespace dotnettask12
{
    class Professor
    {
        //Some standard props for the professor
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime DOB { get; set; }
        public string Field { get; set; }

        //Adding the ICollection interface of student to get a one to many connection from professor to student
        public ICollection<Student> Student { get; set; }
    }
}
