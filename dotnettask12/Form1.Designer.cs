﻿namespace dotnettask12
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.studentBox = new System.Windows.Forms.GroupBox();
            this.btnAddStudent = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.studentDegree = new System.Windows.Forms.TextBox();
            this.studentDate = new System.Windows.Forms.DateTimePicker();
            this.StudentName = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnAddSupervisor = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.supervisorDate = new System.Windows.Forms.DateTimePicker();
            this.specializationSupervisor = new System.Windows.Forms.TextBox();
            this.supervisorName = new System.Windows.Forms.TextBox();
            this.btnAddBoth = new System.Windows.Forms.Button();
            this.studentBox.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // studentBox
            // 
            this.studentBox.Controls.Add(this.btnAddStudent);
            this.studentBox.Controls.Add(this.label3);
            this.studentBox.Controls.Add(this.label2);
            this.studentBox.Controls.Add(this.label1);
            this.studentBox.Controls.Add(this.studentDegree);
            this.studentBox.Controls.Add(this.studentDate);
            this.studentBox.Controls.Add(this.StudentName);
            this.studentBox.Location = new System.Drawing.Point(37, 35);
            this.studentBox.Name = "studentBox";
            this.studentBox.Size = new System.Drawing.Size(364, 350);
            this.studentBox.TabIndex = 0;
            this.studentBox.TabStop = false;
            this.studentBox.Text = "Student";
            // 
            // btnAddStudent
            // 
            this.btnAddStudent.Location = new System.Drawing.Point(78, 301);
            this.btnAddStudent.Name = "btnAddStudent";
            this.btnAddStudent.Size = new System.Drawing.Size(196, 34);
            this.btnAddStudent.TabIndex = 7;
            this.btnAddStudent.Text = "Add Student";
            this.btnAddStudent.UseVisualStyleBackColor = true;
            this.btnAddStudent.Click += new System.EventHandler(this.btnAddStudent_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(25, 204);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 25);
            this.label3.TabIndex = 6;
            this.label3.Text = "Degree";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(25, 121);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(113, 25);
            this.label2.TabIndex = 5;
            this.label2.Text = "Date of birth";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(25, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 25);
            this.label1.TabIndex = 4;
            this.label1.Text = "Name";
            // 
            // studentDegree
            // 
            this.studentDegree.Location = new System.Drawing.Point(25, 241);
            this.studentDegree.Name = "studentDegree";
            this.studentDegree.Size = new System.Drawing.Size(150, 31);
            this.studentDegree.TabIndex = 2;
            // 
            // studentDate
            // 
            this.studentDate.Location = new System.Drawing.Point(25, 156);
            this.studentDate.Name = "studentDate";
            this.studentDate.Size = new System.Drawing.Size(300, 31);
            this.studentDate.TabIndex = 1;
            // 
            // StudentName
            // 
            this.StudentName.Location = new System.Drawing.Point(25, 83);
            this.StudentName.Name = "StudentName";
            this.StudentName.Size = new System.Drawing.Size(150, 31);
            this.StudentName.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnAddSupervisor);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.supervisorDate);
            this.groupBox1.Controls.Add(this.specializationSupervisor);
            this.groupBox1.Controls.Add(this.supervisorName);
            this.groupBox1.Location = new System.Drawing.Point(407, 35);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(381, 350);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Supervisor";
            // 
            // btnAddSupervisor
            // 
            this.btnAddSupervisor.Location = new System.Drawing.Point(77, 301);
            this.btnAddSupervisor.Name = "btnAddSupervisor";
            this.btnAddSupervisor.Size = new System.Drawing.Size(226, 34);
            this.btnAddSupervisor.TabIndex = 8;
            this.btnAddSupervisor.Text = "Add Supervisor";
            this.btnAddSupervisor.UseVisualStyleBackColor = true;
            this.btnAddSupervisor.Click += new System.EventHandler(this.btnAddSupervisor_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(22, 204);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(181, 25);
            this.label7.TabIndex = 7;
            this.label7.Text = "Field of specialization";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(22, 121);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(113, 25);
            this.label6.TabIndex = 6;
            this.label6.Text = "Date of birth";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(22, 40);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(59, 25);
            this.label5.TabIndex = 5;
            this.label5.Text = "Name";
            // 
            // supervisorDate
            // 
            this.supervisorDate.Location = new System.Drawing.Point(22, 156);
            this.supervisorDate.Name = "supervisorDate";
            this.supervisorDate.Size = new System.Drawing.Size(300, 31);
            this.supervisorDate.TabIndex = 4;
            // 
            // specializationSupervisor
            // 
            this.specializationSupervisor.Location = new System.Drawing.Point(22, 241);
            this.specializationSupervisor.Name = "specializationSupervisor";
            this.specializationSupervisor.Size = new System.Drawing.Size(150, 31);
            this.specializationSupervisor.TabIndex = 2;
            // 
            // supervisorName
            // 
            this.supervisorName.Location = new System.Drawing.Point(22, 83);
            this.supervisorName.Name = "supervisorName";
            this.supervisorName.Size = new System.Drawing.Size(150, 31);
            this.supervisorName.TabIndex = 0;
            // 
            // btnAddBoth
            // 
            this.btnAddBoth.Location = new System.Drawing.Point(37, 404);
            this.btnAddBoth.Name = "btnAddBoth";
            this.btnAddBoth.Size = new System.Drawing.Size(723, 34);
            this.btnAddBoth.TabIndex = 2;
            this.btnAddBoth.Text = "Add student with supervisor";
            this.btnAddBoth.UseVisualStyleBackColor = true;
            this.btnAddBoth.Click += new System.EventHandler(this.btnAddBoth_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btnAddBoth);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.studentBox);
            this.Name = "Form1";
            this.Text = "Form1";
            this.studentBox.ResumeLayout(false);
            this.studentBox.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox studentBox;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DateTimePicker studentDate;
        private System.Windows.Forms.TextBox StudentName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox studentDegree;
        private System.Windows.Forms.DateTimePicker supervisorDate;
        private System.Windows.Forms.TextBox specializationSupervisor;
        private System.Windows.Forms.TextBox supervisorName;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnAddStudent;
        private System.Windows.Forms.Button btnAddSupervisor;
        private System.Windows.Forms.Button btnAddBoth;
    }
}

