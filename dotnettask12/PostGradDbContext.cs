﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace dotnettask12
{
    class PostGradDbContext : DbContext
    {
        public DbSet<Student> Student { get; set; }
        public DbSet<Professor> Professor { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //Naming the database dotNetTask12, connecting to the local data source and SQLEXPRESS server
            optionsBuilder.UseSqlServer(@"Data Source=PC7372\SQLEXPRESS;Initial Catalog=dotNetTask12;Integrated Security=True");
        }

    }
        
}
